package main

type DownloadClient interface {
	ListTorrents() (media []Media, err error)
	StartTorrents(torrents []Media)
	StopTorrents(torrents []Media)
	RemoveTorrents(torrents []Media)
}

BIN_NAME=ranga
VERSION=$(shell git describe --always --long --dirty)
BUILD_NUMBER=$(shell date +%d%m%Y_%H%M%S)

build-windows:
	env GOOS=windows GOARCH=amd64 go build -o bin/ranga.exe -ldflags "-s -w -X main.version=${VERSION}.${BUILD_NUMBER}" . 

build-linux:
	env GOOS=linux GOARCH=amd64 go build -o bin/ranga -ldflags "-s -w -X main.version=${VERSION}.${BUILD_NUMBER}" . 

build-darwin:
	env GOOS=darwin GOARCH=amd64 go build -o bin/ranga_darwin -ldflags "-s -w -X main.version=${VERSION}.${BUILD_NUMBER}" . 

all: build-windows build-linux build-darwin
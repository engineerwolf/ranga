module gitlab.com/engineerwolf/rss-godler

go 1.22

require (
	github.com/hekmon/transmissionrpc/v3 v3.0.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/middelink/go-parse-torrent-name v0.0.0-20190301154245-3ff4efacd4c4
	github.com/robfig/cron v1.2.0
	go.uber.org/zap v1.16.0
	gopkg.in/yaml.v2 v2.2.7
)

require (
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hekmon/cunits/v2 v2.1.0 // indirect
	go.uber.org/atomic v1.6.0 // indirect
	go.uber.org/multierr v1.5.0 // indirect
)

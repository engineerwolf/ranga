package main

import (
	"sort"
)

// Media structure to hold media information
type Media struct {
	ID            int64
	OriginalName  string
	Status        MediaStatus
	Name          string
	EpisodeNumber int
	priority      priority
}

type lessFunc func(p1, p2 *Media) bool

// DownloadPrioritySorter sorts slice of Media according to given criteria
type DownloadPrioritySorter struct {
	media []Media
}

func (dps *DownloadPrioritySorter) Len() int {
	return len(dps.media)
}

// Sort part of sort.Sort package
func (dps *DownloadPrioritySorter) Sort(media []Media) {
	dps.media = media
	sort.Sort(dps)
}

func (dps *DownloadPrioritySorter) Swap(i, j int) {
	dps.media[i], dps.media[j] = dps.media[j], dps.media[i]
}

func (dps *DownloadPrioritySorter) Less(i, j int) bool {
	p, q := &dps.media[i], &dps.media[j]

	groupTypeLess := func(p1, p2 *Media) bool {
		return p1.priority.GroupPriority < p2.priority.GroupPriority
	}
	episodePriority := func(p1, p2 *Media) bool {
		return p1.EpisodeNumber < p2.EpisodeNumber
	}
	positionSort := func(p1, p2 *Media) bool {
		return p1.priority.PositionalPriority < p2.priority.PositionalPriority
	}
	var less []lessFunc
	if p.priority.EpisodeOrder {
		less = []lessFunc{groupTypeLess, episodePriority, positionSort}
	} else {
		less = []lessFunc{groupTypeLess, positionSort, episodePriority}
	}
	var k int
	for k = 0; k < len(less)-1; k++ {
		less := less[k]
		switch {
		case less(p, q):
			return true
		case less(q, p):
			return false
		}
	}
	return less[k](p, q)
}

func sliceOfIds(media []Media) []int64 {
	torrentIds := []int64{}
	for _, torrent := range media {
		torrentIds = append(torrentIds, torrent.ID)
	}
	return torrentIds
}

func sliceOfNames(media []Media) []string {
	names := []string{}
	for _, torrent := range media {
		names = append(names, torrent.OriginalName)
	}
	return names
}

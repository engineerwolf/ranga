package main

type cmdOpts struct {
	ConfigFile   string `short:"c" long:"conf" description:"Path to configuration file" default:"./config/ranga.yml"`
	Debug        bool   `short:"d" long:"debug" description:"Enables debug level logging"`
	PrintVersion bool   `short:"v" long:"version" group:"other" description:"Display version number"`
}

type config struct {
	WatchRoot           string              `yaml:"watch"`
	TargetPath          string              `yaml:"target"`
	MaxActive           int                 `yaml:"max-active"`
	Cron                string              `yaml:"cron"`
	GroupOrder          []string            `yaml:"group-ordering"`
	Groups              map[string]group    `yaml:"groups,flow"`
	ClientConfiguration ClientConfiguration `yaml:"clientConfiguration,omitempty"`
}

type group struct {
	Key       string    `yaml:",flow"`
	GroupInfo groupInfo `yaml:",inline"`
}
type groupInfo struct {
	EpisodeOrder bool     `yaml:"episode-ordering"`
	MediaNames   []string `yaml:"names"`
}

type priority struct {
	GroupPriority      int
	EpisodeOrder       bool
	PositionalPriority int
}

// MediaStatus binds torrent status to a status code
type MediaStatus int

const (
	// MediaStatusStopped represents a stopped torrent
	MediaStatusStopped MediaStatus = 0
	// MediaStatusDownloading represents a active torrent
	MediaStatusDownloading MediaStatus = 1
	// MediaStatusDone represents a torrent downloaded
	MediaStatusDone MediaStatus = 2
	// MediaStatusStalled represents a torrent which can't download
	MediaStatusStalled MediaStatus = 3
)

type TransmissionConfiguration struct {
	User     string `yaml:"user,omitempty"`
	Password string `yaml:"password,omitempty"`
	Host     string `yaml:"host,omitempty"`
	Port     string `yaml:"port,omitempty"`
	UseTls   bool   `yaml:"useTls,omitempty"`
}

type ClientConfiguration struct {
	DownloadClient            string                    `yaml:"downloadClient,omitempty"`
	TransmissionConfiguration TransmissionConfiguration `yaml:"transmissionConfiguration,omitempty"`
}

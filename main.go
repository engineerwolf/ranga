package main

import (
	"math"
	"os"
	"os/signal"
	"strings"

	parsetorrentname "github.com/middelink/go-parse-torrent-name"
	"github.com/robfig/cron"
	"go.uber.org/zap"
)

var runtimeConfig config

var log *zap.SugaredLogger

func main() {
	params := os.Args
	cmdOpts := parseCmdOptions(params)
	initLogger(cmdOpts)

	runtimeConfig = readConfigurationFile(cmdOpts.ConfigFile)

	reference := parseReferenceList()

	//TODO: read from file
	trCtr := NewDownloadClient(runtimeConfig.ClientConfiguration)
	reorderTorrents(trCtr, reference)

	c := cron.New()
	c.AddFunc(runtimeConfig.Cron, func() { reorderTorrents(trCtr, reference) })
	go c.Start()
	defer c.Stop()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig
}

func initLogger(cmdOpts cmdOpts) {
	var logger *zap.Logger
	var err error
	if cmdOpts.Debug {
		logger, err = zap.NewDevelopment()
	} else {
		config := zap.NewProductionConfig()
		config.Encoding = "console"
		logger, err = config.Build()
	}
	if err != nil {
		panic(err)
	}
	log = logger.Sugar()
}

func reorderTorrents(trCtr DownloadClient, reference map[string]priority) {
	media, err := trCtr.ListTorrents()
	if err != nil {
		log.Panic("Error listing active torrents", err.Error())
	}
	log.Debugw("got list of torrents", "torrents", media)
	finishedMedia, media := filterFinished(media)

	if len(finishedMedia) > 0 {
		trCtr.RemoveTorrents(finishedMedia)
	}
	parseTorrentNamesList(media)
	assignPriorities(media, reference)
	sortByPriority(media)

	if len(media) < runtimeConfig.MaxActive {
		trCtr.StartTorrents(media)
		log.Debugw("starting torrents", "torrents", media)
	} else {
		trCtr.StartTorrents(media[:runtimeConfig.MaxActive])
		log.Debugw("starting torrents", "torrents", media[:runtimeConfig.MaxActive])
		trCtr.StopTorrents(media[runtimeConfig.MaxActive:])
		log.Debugw("stopping torrents", "torrents", media[runtimeConfig.MaxActive:])
	}
}

func sortByPriority(media []Media) {
	prioritySort := DownloadPrioritySorter{}
	prioritySort.Sort(media)
}

func assignPriorities(media []Media, reference map[string]priority) {
	for i := range media {
		searchPriority(&media[i], reference)
	}
}

func searchPriority(media *Media, reference map[string]priority) {

	for k, v := range reference {
		if media.OriginalName == k || strings.EqualFold(media.Name, k) {
			media.priority = v
			return
		}
	}
	media.priority = priority{GroupPriority: math.MaxInt64, EpisodeOrder: false, PositionalPriority: math.MaxInt64}
	return
}

func parseTorrentNamesList(media []Media) {

	for i := range media {
		parseTorrentName(&media[i])
	}
}

func parseTorrentName(media *Media) {

	info, err := parsetorrentname.Parse(media.OriginalName)
	if err == nil {
		media.Name = info.Title
		media.EpisodeNumber = info.Episode
	}
}

func filterFinished(media []Media) (finished []Media, active []Media) {

	for _, m := range media {
		if m.Status == MediaStatusDone || m.Status == MediaStatusStalled {
			finished = append(finished, m)
		} else {
			active = append(active, m)
		}
	}
	return finished, active
}

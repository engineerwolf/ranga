package main

import (
	"math"
	"reflect"
	"testing"
)

func TestDownloadPrioritySorter_Len(t *testing.T) {
	type fields struct {
		media []Media
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "must run for empty",
			fields: fields{
				media: []Media{},
			},
			want: 0,
		},
		{
			name: "must run for single element",
			fields: fields{
				media: []Media{
					{ID: 1},
				},
			},
			want: 1,
		},
		{
			name: "must run for multiple elements",
			fields: fields{
				media: []Media{
					{ID: 1},
					{ID: 2},
					{ID: 3},
				},
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dps := &DownloadPrioritySorter{
				media: tt.fields.media,
			}
			if got := dps.Len(); got != tt.want {
				t.Errorf("DownloadPrioritySorter.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownloadPrioritySorter_Sort(t *testing.T) {
	type fields struct {
		media []Media
	}
	type args struct {
		media []Media
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dps := &DownloadPrioritySorter{
				media: tt.fields.media,
			}
			dps.Sort(tt.args.media)
		})
	}
}

func Test_sliceOfNames(t *testing.T) {
	type args struct {
		media []Media
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "must run for empty",
			args: args{
				media: []Media{},
			},
			want: []string{},
		},
		{
			name: "must run for single element",
			args: args{
				media: []Media{
					{ID: 1, OriginalName: "Show 1 ep 1"},
				},
			},
			want: []string{"Show 1 ep 1"},
		},
		{
			name: "must run for multiple elements",
			args: args{
				media: []Media{
					{ID: 1, OriginalName: "Show 1 ep 1"},
					{ID: 2, OriginalName: "Show 2 ep 11"},
					{ID: 3, OriginalName: "Show 3 ep 21"},
				},
			},
			want: []string{"Show 1 ep 1", "Show 2 ep 11", "Show 3 ep 21"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sliceOfNames(tt.args.media); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("sliceOfNames() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sliceOfIds(t *testing.T) {
	type args struct {
		media []Media
	}
	tests := []struct {
		name string
		args args
		want []int64
	}{
		{
			name: "must run for empty",
			args: args{
				media: []Media{},
			},
			want: []int64{},
		},
		{
			name: "must run for single element",
			args: args{
				media: []Media{
					{ID: 1, OriginalName: "Show 1 ep 1"},
				},
			},
			want: []int64{1},
		},
		{
			name: "must run for multiple elements",
			args: args{
				media: []Media{
					{ID: 1, OriginalName: "Show 1 ep 1"},
					{ID: 2, OriginalName: "Show 2 ep 11"},
					{ID: 3, OriginalName: "Show 3 ep 21"},
				},
			},
			want: []int64{1, 2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sliceOfIds(tt.args.media); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("sliceOfIds() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownloadPrioritySorter_Less(t *testing.T) {
	type fields struct {
		media []Media
	}
	type args struct {
		i int
		j int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "unspecified priority",
			fields: fields{
				media: []Media{
					{ID: 1},
					{ID: 2},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: false,
		},
		{
			name: "unspecified priority commutitive",
			fields: fields{
				media: []Media{
					{ID: 1},
					{ID: 2},
				},
			},
			args: args{
				i: 1,
				j: 0,
			},
			want: false,
		},
		{
			name: "default priority",
			fields: fields{
				media: []Media{
					{ID: 1, priority: priority{GroupPriority: math.MaxInt64, EpisodeOrder: false, PositionalPriority: math.MaxInt64}},
					{ID: 2, priority: priority{GroupPriority: math.MaxInt64, EpisodeOrder: false, PositionalPriority: math.MaxInt64}},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: false,
		},
		{
			name: "group priority only first element specified",
			fields: fields{
				media: []Media{
					{ID: 1, priority: priority{GroupPriority: 1, EpisodeOrder: false, PositionalPriority: math.MaxInt64}},
					{ID: 2},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: false,
		},
		{
			name: "group priority only second element specified",
			fields: fields{
				media: []Media{
					{ID: 1},
					{ID: 2, priority: priority{GroupPriority: 1, EpisodeOrder: false, PositionalPriority: math.MaxInt64}},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: true,
		},
		{
			name: "group priority descending, EpisodeOrder false",
			fields: fields{
				media: []Media{
					{ID: 1, priority: priority{GroupPriority: 2, EpisodeOrder: true}},
					{ID: 2, priority: priority{GroupPriority: 1, EpisodeOrder: true}},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: false,
		},
		{
			name: "group priority ascending, EpisodeOrder false",
			fields: fields{
				media: []Media{
					{ID: 1, priority: priority{GroupPriority: 1, EpisodeOrder: true}},
					{ID: 2, priority: priority{GroupPriority: 2, EpisodeOrder: true}},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: true,
		},
		{
			name: "group priority ascending, EpisodeOrder true",
			fields: fields{
				media: []Media{
					{ID: 1, EpisodeNumber: 1, priority: priority{GroupPriority: 1, EpisodeOrder: true}},
					{ID: 2, EpisodeNumber: 1, priority: priority{GroupPriority: 2, EpisodeOrder: true}},
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dps := &DownloadPrioritySorter{
				media: tt.fields.media,
			}
			if got := dps.Less(tt.args.i, tt.args.j); got != tt.want {
				t.Errorf("DownloadPrioritySorter.Less() = %v, want %v", got, tt.want)
			}
		})
	}
}

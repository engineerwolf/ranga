package main

import (
	"context"
	"fmt"
	"net/url"

	"github.com/hekmon/transmissionrpc/v3"
)

type TransmissionClient struct {
	tbt *transmissionrpc.Client
}

func NewTransmissionClient(clc ClientConfiguration) DownloadClient {
	client := initializeTransmissionClient(clc.TransmissionConfiguration)
	return &TransmissionClient{
		tbt: client,
	}
}

// AddTorrents implements DownloadClient.
func (t *TransmissionClient) StartTorrents(torrents []Media) {
	ids := sliceOfIds(torrents)
	t.tbt.TorrentStartIDs(context.TODO(), ids)
}

// ListTorrents implements DownloadClient.
func (t *TransmissionClient) ListTorrents() (media []Media, err error) {
	list, err := t.tbt.TorrentGetAll(context.TODO())
	if err != nil {
		return nil, err
	}
	for _, v := range list {
		media = append(media, Media{
			ID:           *v.ID,
			Status:       mediaStatusFromTransmissionStatus(v),
			OriginalName: *v.Name,
		})
	}
	return
}

// RemoveTorrents implements DownloadClient.
func (t *TransmissionClient) RemoveTorrents(torrents []Media) {
	ids := sliceOfIds(torrents)
	payload := transmissionrpc.TorrentRemovePayload{
		IDs:             ids,
		DeleteLocalData: false,
	}
	t.tbt.TorrentRemove(context.TODO(), payload)
}

// StopTorrents implements DownloadClient.
func (t *TransmissionClient) StopTorrents(torrents []Media) {
	ids := sliceOfIds(torrents)
	t.tbt.TorrentStopIDs(context.TODO(), ids)
}

func mediaStatusFromTransmissionStatus(v transmissionrpc.Torrent) MediaStatus {
	if *v.IsFinished {
		return MediaStatusDone
	}
	s := *v.Status
	if *v.IsStalled || s == transmissionrpc.TorrentStatusIsolated {
		return MediaStatusStalled
	}
	if s == transmissionrpc.TorrentStatusStopped {
		return MediaStatusStopped
	}
	if s < transmissionrpc.TorrentStatusSeedWait && s > transmissionrpc.TorrentStatusStopped {
		return MediaStatusDownloading
	}
	if s >= transmissionrpc.TorrentStatusSeedWait {
		return MediaStatusDone
	}
	return MediaStatusDownloading
}

func initializeTransmissionClient(trc TransmissionConfiguration) *transmissionrpc.Client {
	trcUrl := fmt.Sprintf("%s:%s@%s:%s/transmission/rpc",
		trc.User,
		trc.Password,
		trc.Host,
		trc.Port)
	if trc.UseTls {
		trcUrl = "https://" + trcUrl
	} else {
		trcUrl = "http://" + trcUrl
	}
	endpoint, err := url.Parse(trcUrl)
	if err != nil {
		panic(err)
	}
	tbt, err := transmissionrpc.New(endpoint, nil)
	if err != nil {
		panic(err)
	}
	ok, serverVersion, serverMinimumVersion, err := tbt.RPCVersion(context.TODO())
	if err != nil {
		panic(err)
	}
	if !ok {
		panic(fmt.Sprintf("Remote transmission RPC version (v%d) is incompatible with the transmission library (v%d): remote needs at least v%d",
			serverVersion, transmissionrpc.RPCVersion, serverMinimumVersion))
	}
	fmt.Printf("Remote transmission RPC version (v%d) is compatible with our transmissionrpc library (v%d)\n",
		serverVersion, transmissionrpc.RPCVersion)
	return tbt
}

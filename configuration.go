package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/jessevdk/go-flags"
	yaml "gopkg.in/yaml.v2"
)

var version = "undefied"

func parseCmdOptions(params []string) (cmdOpts cmdOpts) {
	_, err := flags.ParseArgs(&cmdOpts, params)

	if err != nil {
		if _, ok := err.(*flags.Error); ok {
			os.Exit(1)
		} else {
			log.Fatalf(err.Error())
		}
	}
	if cmdOpts.PrintVersion {
		fmt.Printf("ranga v.%s\n", version)
		os.Exit(0)
	}
	return
}

func readConfigurationFile(filename string) (config config) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to open file %s", filename)
	}
	err2 := yaml.Unmarshal(data, &config)
	if err2 != nil {
		log.Fatalf("cannot unmarshal data: %v", err2)
	}
	return
}

func parseReferenceList() map[string]priority {

	reference := make(map[string]priority)
	for i, group := range runtimeConfig.GroupOrder {
		episodeOrder := runtimeConfig.Groups[group].GroupInfo.EpisodeOrder
		for j, name := range runtimeConfig.Groups[group].GroupInfo.MediaNames {
			priority := priority{EpisodeOrder: episodeOrder, GroupPriority: i, PositionalPriority: j}
			reference[name] = priority
		}
	}
	return reference
}

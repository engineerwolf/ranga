package main

/*
import (
	"testing"
)
func TestFailingParseLineToMedia(t *testing.T) {

	line := ""
	_, err := parseLineToMedia(line)
	if err == nil {
		t.Errorf("parseLineToMedia(%s): Expected error, got Media", line)
	}
}

func TestParseLineToMedia(t *testing.T) {

	testCases := []struct {
		line           string
		expectedOutput Media
	}{
		{
			"    3     0%       None  Unknown      0.0     0.0   None  Idle         GLOW.S02E02.WEBRip.x264-ION10",
			Media{ID: "3", OriginalName: "GLOW.S02E02.WEBRip.x264-ION10", UpSpeed: 0, DownSpeed: 0, Status: "Idle"},
		},
		{
			"    3     0%       0.22 Kb  Unknown      0.0     0.0   None  Idle         GLOW.S02E02.WEBRip.x264-ION10",
			Media{ID: "3", OriginalName: "GLOW.S02E02.WEBRip.x264-ION10", UpSpeed: 0, DownSpeed: 0, Status: "Idle"},
		},
		{
			"    9    29%   66.02 MB  10 min       6.0   222.0    0.0  Up & Down    Modern.Family.S10E09.WEBRip.x264-ION10",
			Media{ID: "9", OriginalName: "Modern.Family.S10E09.WEBRip.x264-ION10", UpSpeed: 6.0, DownSpeed: 222.0, Status: "Up & Down"},
		},
	}

	for _, testCase := range testCases {
		media, err := parseLineToMedia(testCase.line)
		if err != nil {
			t.Errorf("parseLineToMedia(%s): expected Media, got '%s'", testCase.line, err.Error())
		}
		if media.OriginalName != testCase.expectedOutput.OriginalName {
			t.Errorf("parseLineToMedia(%s): expected '%s', got '%s'", testCase.line, testCase.expectedOutput.OriginalName, media.OriginalName)
		}
		if media.Status != testCase.expectedOutput.Status {
			t.Errorf("parseLineToMedia(%s): expected '%s', got '%s'", testCase.line, testCase.expectedOutput.Status, media.Status)
		}
		if media.UpSpeed != testCase.expectedOutput.UpSpeed {
			t.Errorf("parseLineToMedia(%s): expected %f, got %f", testCase.line, testCase.expectedOutput.UpSpeed, media.UpSpeed)
		}
		if media.DownSpeed != testCase.expectedOutput.DownSpeed {
			t.Errorf("parseLineToMedia(%s): expected %f, got %f", testCase.line, testCase.expectedOutput.DownSpeed, media.DownSpeed)
		}
	}

}
*/

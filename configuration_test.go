package main

import (
	"reflect"
	"testing"
)

func Test_readConfigurationFile(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name       string
		args       args
		wantConfig config
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotConfig := readConfigurationFile(tt.args.filename); !reflect.DeepEqual(gotConfig, tt.wantConfig) {
				t.Errorf("readConfigurationFile() = %v, want %v", gotConfig, tt.wantConfig)
			}
		})
	}
}

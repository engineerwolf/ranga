ARG BUILD_DATE
ARG VERSION
FROM golang:1.22-alpine AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN go build -o /ranga
FROM lsiobase/alpine:3.20-version-d6fdb4e3
LABEL build_version="Engineerwolf version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="engineerwolf"
WORKDIR /
COPY --from=build /ranga /ranga
VOLUME /config
ENTRYPOINT ["/ranga"]
